<?php

namespace Drupal\simple_theme_switch\Theme;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Simple Theme Switch Negotiator.
 */
class SimpleThemeSwitchThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * Variable of the factory for configuration objects.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Variable of the request objects.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Constructs a Switch Theme Negotiator object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    RequestStack $request_stack,
  ) {
    $this->configFactory = $config_factory;
    $this->request = $request_stack->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $this->negotiateRoute($route_match) ? TRUE : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->negotiateRoute($route_match) ?: NULL;
  }

  /**
   * Function that does all of the work in selecting a theme.
   */
  private function negotiateRoute(RouteMatchInterface $route_match): string|false {
    $path_info = $this->request->getPathInfo();
    $theme = FALSE;
    foreach ([
      '/user',
      '/update.php',
    ] as $path) {
      if ($path_info === $path || \strpos($path_info, $path . '/') === 0) {
        $theme = $this->configFactory
          ->get('system.theme')
          ->get('admin');
        break;
      }
    }
    return $theme;
  }

}
